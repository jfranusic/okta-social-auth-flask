import os
import re

import requests

# FIXME: Clean these up!
okta_base_url = os.environ.get('OKTA_BASE_URL')
okta_api_token = os.environ.get('OKTA_API_TOKEN')
okta_facebook_id = os.environ.get('OKTA_FACEBOOK_ID')

headers = {
    'Authorization': 'SSWS {}'.format(okta_api_token),
    'Content-Type': 'application/json',
    'Accept': 'application/json',
}


def list_for(url):
    while(True):
        r = requests.get(url, headers=headers)
        for element in r.json():
            yield element
        if 'link' not in r.headers:
            break
        link_header = r.headers['link']
        links = {}
        for link in link_header.split(','):
            m = re.match('.*\<([^>]+)>;\srel="([^"]+)".*', link)
            url = m.group(1)
            rel = m.group(2)
            links[rel] = url
        if 'next' in links:
            url = links['next']
        else:
            break

url = "{}/api/v1/idps/{}/users".format(okta_base_url, okta_facebook_id)
for user in list_for(url):
    user_id = user['id']
    unlink_url = "{}/api/v1/idps/{}/users/{}".format(
        okta_base_url,
        okta_facebook_id,
        user_id)
    print "DELETE {}".format(unlink_url)
    r = requests.delete(unlink_url, headers=headers)
    print str(r)
