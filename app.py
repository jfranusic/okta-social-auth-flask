import os

from flask import Flask
from flask import render_template
from flask import url_for
from flask.ext.login import LoginManager

from okta_oidc import oidc_blueprint
from okta_social import social

from konfig import Konfig
konf = Konfig()

okta_base_url = konf.okta_base_url
okta_api_token = konf.okta_api_token
okta_client_id = konf.okta_client_id
okta_facebook_id = konf.okta_facebook_id
okta_google_id = konf.okta_google_id
okta_redirect_url = konf.okta_redirect_url

headers = {
    'Authorization': 'SSWS {}'.format(okta_api_token),
    'Content-Type': 'application/json',
    'Accept': 'application/json',
}


app = Flask(__name__)
app.secret_key = okta_api_token
app.config.update(dict(PREFERRED_URL_SCHEME='https'))
app.register_blueprint(oidc_blueprint)
app.register_blueprint(social, url_prefix='/social')

login_manager = LoginManager()
login_manager.setup_app(app)


class UserSession:
    def __init__(self, user_id):
        self.authenticated = True
        self.user_id = user_id

    def is_active(self):
        # In this example, "active" and "authenticated" are the same thing
        return self.authenticated

    def is_authenticated(self):
        # "Has the user authenticated?"
        # See also: http://stackoverflow.com/a/19533025
        return self.authenticated

    def is_anonymous(self):
        return not self.authenticated

    def get_id(self):
        return self.user_id


# Note that this loads users based on user_id
# which is stored in the browser cookie, I think
@login_manager.user_loader
def load_user(user_id):
    # print "Loading user: " + user_id
    return UserSession(user_id)


@app.route("/", methods=['GET', 'POST'])
def hello():
    target_origin = url_for('hello', _external=True).replace('http:', 'https:')
    return render_template(
        'widget_test.html',
        okta_client_id=okta_client_id,
        target_origin=target_origin,
        okta_facebook_id=okta_facebook_id,
        okta_google_id=okta_google_id,
        okta_redirect_url=okta_redirect_url,
        okta_base_url=okta_base_url)


if __name__ == "__main__":
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    if port == 5000:
        app.debug = True
    app.run(port=port)
