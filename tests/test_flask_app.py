import responses
import unittest
import app as flask_app

import json


class TestFlaskApp(unittest.TestCase):

    def setUp(self):
        flask_app.okta_base_url = 'http://example.com'
        flask_app.okta_api_token = 'TEST'
        self.app = flask_app.app.test_client()

        self.session_token = 'E0FC4182-7DD1-11E5-9191-1DF3F5D2EB61'
        self.jit_response_json = {
            'sessionToken': self.session_token,
            '_links': {'finish': {'href': 'http://example.com/finish'}}
        }
        self.tx_id_get_response_json = {
            'externalId': '01234567890123456',
            'profile': {
                'middleName': '',
                'lastName': 'LAST',
                'email': 'first.last@example.com',
                'displayName': 'FIRST LAST',
                'firstName': 'FIRST',
                'profile': ('https://www.facebook.com/'
                            'app_scoped_user_id/01234567890123456/')
            },
            '_links': {
                'idp': {
                    'href': ('https://example.okta.com'
                             '/api/v1/idps/0ab1cdef23ghijKlm4n5')
                }
            }
        }

    def tearDown(self):
        pass

    def test_has_default_route(self):
        path = "/"
        rv = self.app.get(path)
        self.assertEquals("200 OK", rv.status)
        self.assertIn("<html", rv.data)

    def test_can_handle_no_tx_id(self):
        url = '/social/transaction'
        rv = self.app.get(url)
        self.assertIn('action="/social/transaction/-"', rv.data)

    def test_can_handle_tx_id(self):
        uuid = '77745888-7F63-11E5-B67C-033DF6D2EB60'
        url = '/social/transaction?tx_id={}'.format(uuid)
        rv = self.app.get(url)
        action = 'action="/social/transaction/{}"'.format(uuid)
        for string in [uuid, action,
                       '<form id=', 'profileProperties',
                       'John Doe', 'john.doe@example.com',
                       '<button type="submit" value="Submit"']:
            self.assertIn(string, rv.data)

    @responses.activate
    def test_can_handle_tx_id_with_bad_fetch(self):
        uuid = '77745888-7F63-11E5-B67C-033DF6D2EB63'
        responses.add(
            responses.GET,
            'http://example.com/api/v1/idps/tx/{}/source'.format(uuid),
            json={'error': 'error'},
            status=200)

        url = '/social/transaction?tx_id={}'.format(uuid)
        rv = self.app.get(url)
        for string in ['john.doe@example.com']:
            self.assertIn(string, rv.data)

    @responses.activate
    def test_can_handle_tx_id_with_fetch(self):
        uuid = '77745888-7F63-11E5-B67C-033DF6D2EB61'
        responses.add(
            responses.GET,
            'http://example.com/api/v1/idps/tx/{}/source'.format(uuid),
            json=self.tx_id_get_response_json,
            status=200)

        url = '/social/transaction?tx_id={}'.format(uuid)
        rv = self.app.get(url)
        for string in ['FIRST LAST', 'first.last@example.com',
                       'Partner ID matches']:
            self.assertIn(string, rv.data)

    @responses.activate
    def test_can_handle_post(self):
        # Setup test
        uuid = 'AEEEEF76-7DC4-11E5-94FF-966DBE0520A2'
        responses.add(
            responses.POST,
            'http://example.com/api/v1/idps/tx/{}/lifecycle/jit'.format(uuid),
            json=self.jit_response_json,
            status=200)

        url = '/social/transaction/{}'.format(uuid)
        rv = self.app.post(url)
        # Test that I POST to: {}/api/v1/idps/tx/{}/lifecycle/jit
        self.assertIn(uuid, responses.calls[0].request.url)
        self.assertEquals(200, rv.status_code)
        # Test that I return redirect to the "finish" link
        for string in ['finishTxForm', '<form id=', 'sessionToken',
                       'http://example.com/finish', self.session_token]:
            self.assertIn(string, rv.data)

    @responses.activate
    def test_properly_posts_profile(self):
        # Setup test
        uuid = '8100B47A-7DC7-11E5-9038-84A3F0F8B08E'
        responses.add(
            responses.POST,
            'http://example.com/api/v1/idps/tx/{}/lifecycle/jit'.format(uuid),
            json=self.jit_response_json,
            status=200)
        url = '/social/transaction/{}'.format(uuid)
        data = {
            'mobilePhone': '424-562-2516',
            'secondEmail': 'john.doe@example.com',
            'promo_code': 'ABCD'
            }

        self.app.post(url, data=data)
        # Test that I POST to: {}/api/v1/idps/tx/{}/lifecycle/jit
        self.assertIn(uuid, responses.calls[0].request.url)
        expected = {
            "secondEmail": "john.doe@example.com",
            "mobilePhone": "424-562-2516",
            "promo_code": "ABCD"
            }
        body = responses.calls[0].request.body
        print body
        actual = json.loads(body)
        # Test that I POST'ed this:
        #   {
        #       "profile": {
        #        "secondEmail": "john.doe@example.com",
        #        "mobilePhone": "424-562-2516",
        #        "promo_code": "ABCD"
        #       }
        #   }
        for key in ['secondEmail', 'mobilePhone', 'promo_code']:
            self.assertEquals(actual['profile'][key], expected[key])
