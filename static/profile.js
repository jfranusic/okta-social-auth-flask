var x = "";
var y = ""
$( document ).ready(function() {
    $.ajax({
	type: "GET",
	dataType: 'json',
	url: "/users/profile",
	success: function(data){
            setupPreferences(data.profile);
	}
    });
});

function setupPreferences(data) {
    console.log(data);
    y = data;
    $("option").each(function() {
	if ( $(this).attr('value') === data.languagePreference ) {
	    $(this).prop('selected', true);
	}
    });
    $("[name='emailEnabled']").prop('checked', Boolean(data.emailEnabled == 'on'));
    $("form").show();
}
