var redirectUrl = '{{okta_base_url}}/oauth2/v1/widget/callback?targetOrigin={{target_origin}}';
var oktaSignIn = function() {
  var baseUrl = '{{okta_base_url}}';
  return new OktaSignIn({
    baseUrl: baseUrl,
    clientId: '{{okta_client_id}}',
    redirectUri: '{{okta_base_url}}/oauth2/v1/widget/callback?targetOrigin={{target_origin}}',
    authScheme: 'OAUTH2',
    authParams: {
      responseType: 'id_token',
      // scope: [ 'openid', 'email', 'profile', 'address', 'phone' ]
      scope: [ 'openid', 'email', 'profile', 'address', 'phone' ]
    },
    idps: [
      {
        'type': 'FACEBOOK',
        'id': '{{okta_facebook_id}}'
      }
    ]
  });
}();

oktaSignIn.renderEl(
  { el: '#container' },
 function (res) {
    if (res.status === 'SUCCESS') {
      // console.log('User %s succesfully authenticated %o', res.user.profile.login, res.user);
      console.log(res);
      // window.location.href=redirectUrl;
    }
  },
 function (err) { console.log('Unexpected error authenticating user: %o', err); }
);
