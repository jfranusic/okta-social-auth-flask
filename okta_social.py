import pprint
import os

from flask import Blueprint
from flask import render_template
from flask import request
from flask import flash
from flask import url_for
import requests
import json

# FIXME: Clean these up!
okta_base_url = os.environ.get('OKTA_BASE_URL')
okta_api_token = os.environ.get('OKTA_API_TOKEN')
okta_client_id = os.environ.get('OKTA_CLIENT_ID')
okta_facebook_id = os.environ.get('OKTA_FACEBOOK_ID')

headers = {
    'Authorization': 'SSWS {}'.format(okta_api_token),
    'Content-Type': 'application/json',
    'Accept': 'application/json',
}

social = Blueprint('social_blueprint', __name__)


@social.route('/callout/account')
def callout_account():
    return account()


@social.route('/callout/provisioning')
def callout_provisioning():
    return tx()


def get_profile(tx_id_input='-'):
    tx_id = request.args.get('tx_id', tx_id_input)
    print "tx_id: " + tx_id
    action = url_for('.tx_post', tx_id=tx_id)
    source_user = {
        "externalId": "151256125265525",
        "profile": {
            "middleName": "Augustus",
            "lastName": "Lindbergh",
            "email": "charles.augustus.lindberg02@mailinator.com",
            "displayName": "Charles Augustus Lindbergh",
            "firstName": "Charles",
            "profile": "https://www.facebook.com/app_scoped_user_id/1234/"
        },
        "_links": {
            "idp": {
                "href": "https://examle.oktapreview.com/api/v1/idps/0oa4sdtq54cbbdSeo0h7"
            }
        }
    }

    rv = None
    url = '{}/api/v1/idps/tx/{}/source'.format(
        okta_base_url,
        tx_id)

    if tx_id != '-':
        r = requests.get(url, headers=headers)
        try:
            rv = r.json()
            print "Got: " + rv
        except:
            pass
    if rv and 'profile' in rv:
        source_user = rv

    user = source_user['profile']
    return (user, action)


def get_profile_old():
    tx_id = request.args.get('tx_id', '-')
    print "tx_id: " + tx_id
    action = url_for('.tx_post', tx_id=tx_id)
    user = {
        'firstName': 'John',
        'lastName': 'Doe',
        'email': 'john.doe@example.com'
        }
    rv = None
    url = '{}/api/v1/idps/tx/{}/source'.format(
        okta_base_url,
        tx_id)

    if tx_id != '-':
        r = requests.get(url, headers=headers)
        try:
            rv = r.json()
            print "Got: " + rv
        except:
            pass
    if rv and 'profile' in rv:
        user = rv['profile']
    return (user, action)


@social.route('/transaction')
def tx():
    (user, action) = get_profile()
    return render_template('profile_prompt.html',
                           user=user,
                           action=action)


@social.route('/account')
def account():
    (user, action) = get_profile()
    return render_template('account_prompt.html',
                           user=user,
                           action=action)


@social.route('/prompt')
def prompt():
    tx_id = request.args.get('tx_id', '-')
    state = request.args.get('state', 'mocked')
    (user, action) = get_profile()
    pprint.pprint(user)
    account_url = url_for('.account', tx_id=tx_id, state=state)
    tx_url = url_for('.tx', tx_id=tx_id, state=state)
    return render_template('create_or_link.html',
                           user=user,
                           action=action,
                           account_url=account_url,
                           tx_url=tx_url,
                           tx_id=tx_id)


@social.route('/link/<tx_id>', methods=['POST'])
def link_account(tx_id):
    url = '{}/api/v1/idps/tx/{}/lifecycle/provision'.format(
        okta_base_url,
        tx_id)

    # Put all POST parameters into the "profile" JSON payload
    params = request.form.copy()
    pprint.pprint(params)
    payload = {'profile': {}}
    for key in params.keys():
        payload['profile'][key] = params[key]

    print "POSTing to URL: '{}'".format(url)
    r = requests.post(url, headers=headers, data=json.dumps(payload))
    rv = r.json()
    pprint.pprint(rv)

    finish_tx_form = None
    if 'next' not in rv['_links']:
        raise Exception('No "next" link found in "_links"')
    else:
        finish_tx_form = rv['_links']['next']['href']

    return render_template('tx_finish.html',
                           finishTxForm=finish_tx_form,
                           sessionToken=rv['sessionToken'])


@social.route('/transaction/<tx_id>', methods=['POST'])
def tx_post(tx_id):
    authn_url = '{}/api/v1/authn'.format(
        okta_base_url)
    payload = {
        'username': request.form.get('username'),
        'password': request.form.get('password'),
        }
    r = requests.post(authn_url, headers=headers, data=json.dumps(payload))
    result = r.json()
    if 'errorCode' in result:
        (user, action) = get_profile(tx_id)
        flash(result['errorSummary'])
        return render_template('account_prompt.html',
                               user=user,
                               action=action)
    user_id = result['_embedded']['user']['id']
    url = '{}/api/v1/idps/tx/{}/lifecycle/confirm/{}'.format(
        okta_base_url,
        tx_id,
        user_id)

    print "POSTing to URL: '{}'".format(url)
    payload = {}
    r = requests.post(url, headers=headers, data=json.dumps(payload))
    rv = r.json()
    pprint.pprint(rv)

    finish_tx_form = None
    if 'next' not in rv['_links']:
        raise Exception('No "next" link found in "_links"')
    else:
        finish_tx_form = rv['_links']['next']['href']

    return render_template('tx_finish.html',
                           finishTxForm=finish_tx_form,
                           sessionToken=rv['sessionToken'])
